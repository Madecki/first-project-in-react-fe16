import React from 'react';
import AboutEmployee from './AboutEmployee/AboutEmployee';
import './About.css';

const employees = [
  {
    name: 'Ewelina',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.',
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Jessica_Ennis_%28May_2010%29_cropped.jpg/180px-Jessica_Ennis_%28May_2010%29_cropped.jpg'
  },
  {
    name: 'Grzegorz',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.',
    img: 'https://static.antyweb.pl/wp-content/uploads/2019/03/07154101/superman-1420x670.jpg'
  },
  {
    name: 'Magda',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.',
    img: 'https://static01.nyt.com/images/2019/11/17/books/review/17Salam/Salam1-superJumbo.jpg'
  },
  {
    name: 'Natan',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.',
    img: ''
  },
  {
    name: 'Sebastian',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.',
    img: undefined
  },
  {
    name: 'Wojtciech',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.',
    img: 'https://www.euro.com.pl/fckeditor/image/opisy/gry/spider-man/spider-man.jpg'
  },
]

function About () {
  return (
    <section className="about" id="about">
    <div className="container">
      <h1>Nasi specjaliści</h1>
      {
        employees.map(employee => {
          return <AboutEmployee name={employee.name} desc={employee.desc} img={employee.img} />
        })
      }
    </div>
  </section>
  )
}

export default About;